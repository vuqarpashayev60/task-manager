package com.task.util;
import com.task.domain.RegistrationForm;
import com.task.domain.User;

public class RegistrationFormUtil {

    public static User convert(RegistrationForm form) {
        User user = new User();
        user.setName(form.getName());
        user.setSurName(form.getSurName());
        user.setUserName(form.getUserName());
        user.setPassword(form.getPassword());
        return user;
    }
}