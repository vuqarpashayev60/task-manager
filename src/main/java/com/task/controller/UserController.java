package com.task.controller;

import com.task.domain.RegistrationForm;
import com.task.domain.Task;
import com.task.domain.User;
import com.task.service.TaskService;
import com.task.service.UserService;
import com.task.util.RegistrationFormUtil;
import com.task.validator.RegistrationFormValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Autowired
    RegistrationFormValidator registrationFormValidator;

    @Autowired
    TaskService taskService;

    @Autowired
    private MessageSource messageSource;


    @InitBinder("registrationForm")
    public void initBinder(WebDataBinder dataBinder) {
        if (Objects.requireNonNull(dataBinder.getTarget()).getClass() == RegistrationForm.class) {
            dataBinder.setValidator(registrationFormValidator);
        }
    }

    @PostMapping("/register")
    public User userAdd(@Validated @RequestBody RegistrationForm registrationForm,
                        BindingResult errors) {
        User user;
        if (errors.hasErrors()) {
            List<String> errorList = new ArrayList<>();
            errors.getAllErrors().forEach(objectError -> {
                String msg = messageSource.getMessage(objectError.getCode(), null, Locale.getDefault());
                errorList.add(msg);
            });
            logger.error("Field Error" + errorList.toString());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorList.toString());
        } else {
            try {
                user = RegistrationFormUtil.convert(registrationForm);
                userService.addUser(user);
                logger.info("Insert Processing Successful");
            } catch (Exception e) {
                logger.error("Registration process error" + e);
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
            }
        }
        return user;
    }

    @GetMapping("/{id}/tasks")
    public List<Task> getTaskListByUserId(@PathVariable(name = "id") long userId) {
        List<Task> userTaskList;
        try {
            Optional<User> optionalUser = userService.getUserById(userId);
            if (optionalUser.isPresent()) {
                userTaskList = taskService.getTaskListByUserId(userId);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + userId + " not found");
            }
        } catch (Exception e) {
            logger.error("Server Error : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error");
        }
        return userTaskList;
    }

    @GetMapping("/deactiveuserlist")
    public List<User> getDeactivatedUserList() {
        List<User> deactivateUserList = null;
        try {
            deactivateUserList = userService.getDeactiveUserList();
        } catch (Exception e) {
            logger.debug("Server Error : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error");
        }
        return deactivateUserList;
    }

    @PutMapping("/activateUser/{userId}")
    public void activateUser(@PathVariable(name = "userId") long userID) {
        try {
            userService.userActivateStatus(userID);
        } catch (Exception e) {
            logger.debug("Activation Error : "+e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error");
        }
    }
}
