package com.task.controller;

import com.task.domain.PaginationResult;
import com.task.domain.Task;
import com.task.service.TaskService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.Optional;

@RestController
@RequestMapping("/task/")
public class TaskController {
    private Logger logger = Logger.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;


    @GetMapping("/")
    public PaginationResult<Task> getAllTaskList(
            @RequestParam(name = "limit", required = false, defaultValue = "10") int limit,
            @RequestParam(name = "offset", required = false, defaultValue = "0") int offset
    ) {
        PaginationResult<Task> paginationResult;
        try {
            paginationResult = taskService.getAllTaskList(limit, offset);
            logger.info("Task list take process successful");
        } catch (Exception e) {
            logger.error("Server Error : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error");
        }
        return paginationResult;
    }

    @PostMapping("/")
    public Task addTask(@RequestBody Task task) {
        try {
            task = taskService.addTask(task);
            logger.info("Task add process successful");
        } catch (Exception e) {
            logger.error("Server Error : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error");
        }
        return task;
    }


    @GetMapping("/{id}")
    public Task getTaskById(@PathVariable(name = "id") long id) {
        Task task;

        try {
            Optional<Task> optionalTask = taskService.getTaskById(id);
            if (optionalTask.isPresent()) {
                task = optionalTask.get();
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Task with id " + id + " not found");
            }
        } catch (Exception e) {
            logger.error("Server Error : " + e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error");
        }
        return task;
    }


}
