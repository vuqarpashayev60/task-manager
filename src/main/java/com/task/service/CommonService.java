package com.task.service;

public interface CommonService {
    boolean checkUserName(String userName);

    boolean checkTitle(String title);
}
