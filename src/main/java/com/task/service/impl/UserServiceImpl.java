package com.task.service.impl;

import com.task.domain.Task;
import com.task.domain.User;
import com.task.repository.TaskRepository;
import com.task.repository.UserRepository;
import com.task.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    @Override
    public User addUser(User user) {
        String password = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(password);
        return userRepository.addUser(user);
    }

    @Override
    public Optional<User> getUserById(long id) {
        return userRepository.getUserById(id);
    }

    @Override
    public Optional<User> getUserByUserName(String userName) {
        logger.debug("User Service User Name :" + userName);
        return userRepository.getUserByUSerName(userName);
    }

    @Override
    public List<User> getDeactiveUserList() {
        return userRepository.getDeactiveUserList();
    }

    @Transactional
    @Override
    public User userActivateStatus(long userId) {
        Task task = taskRepository.getTaskListByUserId(userId).get(0);

        LocalDateTime starttingDate = task.getStartingDate();
        LocalDateTime expireDate = task.getExpirationDate();

        Duration taskDuration = Duration.between(starttingDate, expireDate);

        LocalDateTime endDate = expireDate.plus(taskDuration);
        userRepository.activateUser(userId, Timestamp.valueOf(endDate));
        userRepository.deleteBlacklistByUserId(userId);
        userRepository.changeTaskStatus(endDate, userId);
        return null;
    }
}
