package com.task.service.impl;

import com.task.repository.CommonRepository;
import com.task.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonServiceImpl implements CommonService {
   @Autowired
    CommonRepository commonRepository;

    @Override
    public boolean checkUserName(String userName) {
        return commonRepository.checkUserName(userName);
    }

    @Override
    public boolean checkTitle(String title) {
        return commonRepository.checkTitle(title);
    }
}
