package com.task.service.impl;

import com.task.domain.PaginationResult;
import com.task.domain.Task;
import com.task.domain.User;
import com.task.repository.TaskRepository;
import com.task.repository.UserRepository;
import com.task.service.TaskService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    Logger logger = Logger.getLogger(TaskServiceImpl.class);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;


    @Transactional
    @Override
    public Task addTask(Task task) {
        task = taskRepository.addTask(task);
        return task;
    }

    @Override
    public PaginationResult<Task> getAllTaskList(int limit, int offset) {
        PaginationResult<Task> paginationResult = null;

        List<Task> taskList = taskRepository.getAllTaskListLimited(limit, offset);
        int total = taskRepository.countAllTasks();

        paginationResult = new PaginationResult<>();

        paginationResult.setList(taskList);
        paginationResult.setLimit(limit);
        paginationResult.setOffset(offset);
        paginationResult.setTotal(total);
        paginationResult.setFiltered(taskList.size());

        return paginationResult;
    }


    @Override
    public List<Task> getTaskListByUserId(long userId) {
        return  taskRepository.getTaskListByUserId(userId);
    }

    @Override
    public Optional<Task> getTaskById(long id) {
        return taskRepository.getTaskById(id);
    }

    @Transactional
    @Override
    public void expiryProcess() {
        List<Task> taskList = taskRepository.getAllTaskList();
       for (Task task : taskList) {
            if (task.getExpirationDate().isBefore(LocalDateTime.now())) {
                taskRepository.finishTask(task.getId());
                taskRepository.deactivateUser(task.getUser().getId());
                taskRepository.addBlackList(task.getUser().getId());
            }
        }
    }
}
