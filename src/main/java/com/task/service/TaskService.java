package com.task.service;

import com.task.domain.PaginationResult;
import com.task.domain.Task;

import java.util.List;
import java.util.Optional;

public interface TaskService {
    Task addTask(Task task);

    PaginationResult<Task> getAllTaskList(int limit, int offset);

    List<Task> getTaskListByUserId(long userId);

    Optional<Task> getTaskById(long id);

    void expiryProcess();
}

