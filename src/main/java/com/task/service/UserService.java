package com.task.service;

import com.task.domain.User;
import java.util.List;
import java.util.Optional;

public interface UserService {
    User addUser(User user);

    Optional<User> getUserById(long id);

    Optional<User> getUserByUserName(String userName);

    List<User> getDeactiveUserList();

    User userActivateStatus(long userId);
}
