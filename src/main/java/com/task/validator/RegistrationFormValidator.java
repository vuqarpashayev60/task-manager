package com.task.validator;

import com.task.domain.RegistrationForm;
import com.task.service.CommonService;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.task.validator.RegisterValidationConstants.*;

@Component
public class RegistrationFormValidator implements Validator {

    @Autowired
    CommonService commonService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == RegistrationForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationForm form = (RegistrationForm) target;
        String regexAzName = "^[a-zA-ZöüƏəşŞçÇĞğÖÜ]+$";


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "registrationForm.name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surName", "registrationForm.surName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userName", "registrationForm.userName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "registrationForm.password.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordConfirm", "registrationForm.password-confirm.required");


        if (!errors.hasErrors()) {
            if (!GenericValidator.isInRange(form.getName().length(), NAME_MIN_LENGTH, NAME_MAX_LENGTH)) {
                errors.rejectValue("name", "registrationForm.name.length");
            }
            if (!GenericValidator.matchRegexp(form.getName(), regexAzName)) {
                errors.rejectValue("name", "registrationForm.name.invalid");
            }
            if (!GenericValidator.isInRange(form.getSurName().length(), SURNAME_MIN_LENGTH, SURNAME_MAX_LENGTH)) {
                errors.rejectValue("surName", "registrationForm.surName.length");
            }

            if (!GenericValidator.matchRegexp(form.getSurName(), regexAzName)) {
                errors.rejectValue("surName", "registrationForm.surName.invalid");
            }
            if (commonService.checkUserName(form.getUserName())) {
                errors.rejectValue("userName", "registrationForm.userName.duplicate");
            }

            if (!errors.hasFieldErrors("password") && !errors.hasFieldErrors("passwordConfirmation")) {
                if (!form.getPassword().equals(form.getPasswordConfirm())) {
                    errors.rejectValue("passwordConfirm", "registrationForm.password.mismatch");
                }
            }
        }


    }

}
