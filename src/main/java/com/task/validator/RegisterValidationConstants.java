package com.task.validator;

public class RegisterValidationConstants {
    public static final String NAME = "name";
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 45;

    public static final String SURNAME = "surname";
    public static final int SURNAME_MIN_LENGTH = 3;
    public static final int SURNAME_MAX_LENGTH = 45;

    public static final String USERNAME = "username";
    public static final int USERNAME_MIN_LENGTH = 3;
    public static final int USERNAME_MAX_LENGTH = 100;

    public static final String PHONE="phone";
    public static final int PHONE_MIN_LENGTH=10;
    public static final int PHONE_MAX_LENGTH=13;


}
