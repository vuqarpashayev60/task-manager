package com.task.scheduler;

import com.task.service.TaskService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TaskSchedule {
    private Logger logger = Logger.getLogger(TaskSchedule.class);

    @Autowired
    TaskService taskService;

    @Scheduled(fixedRateString = "600000")
    public void processNotificationQueue() {
       try {
            taskService.expiryProcess();
            logger.debug("Schedule process successful");
        } catch (Exception e) {
            logger.error("Server Error" + e);
        }
    }
}
