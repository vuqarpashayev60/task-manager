package com.task.rowmapper;

import com.task.constant.TaskStatus;
import com.task.constant.UserRole;
import com.task.constant.UserStatus;
import com.task.domain.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int i) throws SQLException {
        User user = new User();
        user.setId(rs.getLong("id"));
        user.setName(rs.getString("name"));
        user.setSurName(rs.getString("surname"));
        user.setUserName(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setUserStatus(UserStatus.from(rs.getInt("user_status")));
        user.setUserRole(UserRole.from(rs.getInt("user_role")));
        user.setRegistrationDate(rs.getTimestamp("registration_date").toLocalDateTime());
        if (rs.getTimestamp("udate") != null) {
            user.setUdate(rs.getTimestamp("udate").toLocalDateTime());
        }
        user.setIdate(rs.getTimestamp("idate").toLocalDateTime());
        return user;
    }
}
