package com.task.rowmapper;

import com.task.constant.TaskStatus;
import com.task.constant.UserRole;
import com.task.constant.UserStatus;
import com.task.domain.Task;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TaskRowMapper implements RowMapper<Task> {

    @Override
    public Task mapRow(ResultSet rs, int i) throws SQLException {
        Task task = new Task();
        task.setId(rs.getLong("t.id"));
        task.setTitle(rs.getString("t.title"));
        task.setStartingDate(rs.getTimestamp("t.starting_date").toLocalDateTime());
        task.setExpirationDate(rs.getTimestamp("t.expiration_date").toLocalDateTime());
        task.setTaskStatus(TaskStatus.from(rs.getInt("t.task_status")));
        if (rs.getTimestamp("t.udate") != null) {
            task.setUdate(rs.getTimestamp("t.udate").toLocalDateTime());
        }
        task.setIdate(rs.getTimestamp("t.idate").toLocalDateTime());
        task.getUser().setId(rs.getLong("u.id"));
        task.getUser().setName(rs.getString("u.name"));
        task.getUser().setSurName(rs.getString("u.surname"));
        task.getUser().setUserName(rs.getString("u.username"));
        task.getUser().setPassword(null);
        task.getUser().setUserStatus(UserStatus.from(rs.getInt("u.user_status")));
        task.getUser().setUserRole(UserRole.from(rs.getInt("u.user_role")));
        task.getUser().setRegistrationDate(rs.getTimestamp("u.registration_date").toLocalDateTime());
        if (rs.getTimestamp("u.udate") != null) {
            task.getUser().setUdate(rs.getTimestamp("u.udate").toLocalDateTime());
        }
        task.getUser().setIdate(rs.getTimestamp("u.idate").toLocalDateTime());
        return task;
    }
}
