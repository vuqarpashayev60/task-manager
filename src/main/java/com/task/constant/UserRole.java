package com.task.constant;


import java.util.Arrays;

public enum UserRole {
    USER(1), ADMIN(2);

    UserRole(int id) {
        this.id = id;
    }

    private int id;

    public int getId() {
        return id;
    }

    public static UserRole from(int role) {
        return Arrays.stream(values())
                .filter(f -> f.id == role)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown user role " + role));
    }

}
