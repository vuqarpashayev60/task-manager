package com.task.constant;

import java.util.Arrays;

public enum TaskStatus {
    STARTED(0), FINISHED(1);

    TaskStatus(int status) {
        this.status = status;
    }

    private int status;

    public int getStatus() {
        return status;
    }

    public static TaskStatus from(int status) {
        return Arrays.stream(values())
                .filter(f -> f.status == status)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown user status " + status));
    }
}
