package com.task.constant;

import org.apache.log4j.Logger;

import java.util.Arrays;

public enum UserStatus {

    DEACTIVE(0), ACTIVE(1);


    UserStatus(int status) {
        this.status = status;
    }

    private int status;

    public int getStatus() {
        return status;
    }

    public static UserStatus from(int status) {
        Logger logger = Logger.getLogger(UserStatus.class);
        logger.debug("Role Status : "+status);
        return Arrays.stream(values())
                .filter(f -> f.status == status)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown user status " + status));
    }


}
