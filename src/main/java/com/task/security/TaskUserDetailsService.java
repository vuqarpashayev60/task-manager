package com.task.security;

import com.task.domain.User;
import com.task.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaskUserDetailsService implements UserDetailsService {
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserPrincipal userPrincipal = null;
        Optional<User> userOptional = userService.getUserByUserName(username);
       if (userOptional.isPresent()) {
            User user = userOptional.get();
            userPrincipal = new UserPrincipal(user);
        } else {
            throw new UsernameNotFoundException("User " + username + " not found!");
        }
        return userPrincipal;
    }
}
