package com.task.repository;

import com.task.domain.User;

public interface CommonRepository {
    boolean checkUserName(String userName);

    boolean checkTitle(String title);

}
