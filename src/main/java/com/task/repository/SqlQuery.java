package com.task.repository;

public class SqlQuery {
    public static final String CHECK_USERNAME = "SELECT count(id) FROM taskproject.user WHERE username=? AND status=1";

    public static final String CHECK_TITLE = "SELECT count(id) FROM taskproject.task WHERE title=? AND status=1";

    public static final String REGISTER_USER = "INSERT INTO taskproject.user " +
            "(name, surname, username, password, user_status, user_role, registration_date, idate, status) " +
            "VALUES " +
            "(?, ?, ?, ?, 1, 1, current_timestamp(), current_timestamp(), 1) ";

    public static final String USER_LIST_BY_ID = "SELECT id, name, surname, username, NULL AS password, user_status, user_role, registration_date, idate, udate FROM  taskproject.user WHERE id=? AND status=1 ";

    public static final String USER_LIST_BY_USER_NAME = "SELECT id, name, surname, username, password, user_status, user_role, registration_date, idate, udate FROM  taskproject.user WHERE username=? AND status=1 ";

    public static final String DEACTIVATED_USER_LIST = "SELECT id, name, surname, username, NULL AS password, user_status, user_role, registration_date, idate, udate FROM taskproject.user WHERE user_status=0 ";

    public static final String ADD_TASK = "INSERT INTO taskproject.task " +
            "(user_id, title,starting_date, expiration_date, task_status, idate, status) " +
            "VALUES " +
            "(?, ?, current_timestamp(), ?, 0, current_timestamp(), 1) ";

    public static final String GET_ALL_TASK_LIST_LIMITED = "SELECT  " +
            "    t.id, " +
            "    t.title, " +
            "    t.starting_date, " +
            "    t.expiration_date, " +
            "    t.task_status, " +
            "    t.udate, " +
            "    t.idate, " +
            "    u.id, " +
            "    u.name, " +
            "    u.surname, " +
            "    u.username, " +
            "    u.password, " +
            "    u.user_status, " +
            "    u.user_role, " +
            "    u.registration_date, " +
            "    u.idate, " +
            "    u.udate " +
            "FROM " +
            "    taskproject.task t " +
            "        INNER JOIN " +
            "    taskproject.user u ON u.id = t.user_id AND u.status = 1 " +
            "WHERE " +
            "t.status = 1 " +
            "ORDER BY t.id DESC " +
            "LIMIT ? OFFSET ? ";


    public static final String GET_TASK_BY_ID = "SELECT  " +
                "    t.id, " +
                "    t.title, " +
                "    t.starting_date, " +
                "    t.expiration_date, " +
                "    t.task_status, " +
                "    t.udate, " +
                "    t.idate, " +
                "    u.id, " +
                "    u.name, " +
                "    u.surname, " +
                "    u.username, " +
                "    u.password, " +
                "    u.user_status, " +
                "    u.user_role, " +
                "    u.registration_date, " +
                "    u.idate, " +
                "    u.udate " +
                "FROM " +
                "    taskproject.task t " +
                "        INNER JOIN " +
                "    taskproject.user u ON u.id = t.user_id AND u.status = 1 " +
                "WHERE " +
                "t.status = 1 AND t.id=? ";


    public static final String GET_TASK_LIST_BY_USER_ID =
            "SELECT  " +
                    "    t.id, " +
                    "    t.title, " +
                    "    t.starting_date, " +
                    "    t.expiration_date, " +
                    "    t.task_status, " +
                    "    t.udate, " +
                    "    t.idate, " +
                    "    u.id, " +
                    "    u.name, " +
                    "    u.surname, " +
                    "    u.username, " +
                    "    u.password, " +
                    "    u.user_status, " +
                    "    u.user_role, " +
                    "    u.registration_date, " +
                    "    u.idate, " +
                    "    u.udate " +
                    "FROM " +
                    "    taskproject.task t " +
                    "        INNER JOIN " +
                    "    taskproject.user u ON u.id = t.user_id AND u.status = 1 " +
                    "WHERE " +
                    "t.status = 1 AND t.user_id=? ";

   public static final String GET_ACTIVE_STATUS_TASK_LIST =
        "SELECT  " +
                "    t.id, " +
                "    t.title, " +
                "    t.starting_date, " +
                "    t.expiration_date, " +
                "    t.task_status, " +
                "    t.udate, " +
                "    t.idate, " +
                "    u.id, " +
                "    u.name, " +
                "    u.surname, " +
                "    u.username, " +
                "    u.password, " +
                "    u.user_status, " +
                "    u.user_role, " +
                "    u.registration_date, " +
                "    u.idate, " +
                "    u.udate " +
                "FROM " +
                "    taskproject.task t " +
                "        INNER JOIN " +
                "    taskproject.user u ON u.id = t.user_id AND u.status = 1 " +
                "WHERE " +
                "t.status = 1 AND t.task_status=0 ";

    public static final String GET_COUNT_TASK = "SELECT count(id) FROM taskproject.task ";

    public static final String DEACTIVATE_USER_STATUS = "UPDATE taskproject.user SET user_status=0 WHERE id=? ";

    public static final String FINISHE_TASK_STATUS = "UPDATE taskproject.task SET task_status=1 WHERE id=? ";

    public static final String ADD_BLACK_LIST = "INSERT INTO taskproject.blacklist (user_id) VALUE (?) ";

    public static final String CHANGE_USER_STATUS = "UPDATE taskproject.user SET user_status=1 WHERE id=? AND status=1 ";

    public static final String DELETE_USER_FROM_BLACKLIST = "UPDATE taskproject.blacklist SET status=0, udate=CURRENT_TIMESTAMP WHERE user_id=? AND status=1 ";

    public static final String CHANGE_TASK_STATUS = "UPDATE taskproject.task SET expiration_date=?, task_status=0, udate=CURRENT_TIMESTAMP WHERE user_id=? AND status=1 ";

}
