package com.task.repository;

import com.task.domain.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository {
    Task addTask(Task task);

    List<Task> getAllTaskListLimited(int limit, int offset);

    List<Task> getAllTaskList();

    Integer countAllTasks();

    boolean deactivateUser(long userId);

    boolean finishTask(long taskId);

    List<Task> getTaskListByUserId(long userId);

    Optional<Task> getTaskById(long id);

    void addBlackList(long userId);
}
