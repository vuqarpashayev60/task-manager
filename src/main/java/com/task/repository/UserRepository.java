package com.task.repository;

import com.task.domain.User;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface UserRepository {

    User addUser(User user);

    Optional<User> getUserById(long id);

    Optional<User> getUserByUSerName(String userName);

    List<User>getDeactiveUserList();

    void activateUser(long userId, Timestamp expireDate);

    void deleteBlacklistByUserId(long userId);

    void changeTaskStatus(LocalDateTime expirationDate, long userId);


}
