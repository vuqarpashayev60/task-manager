package com.task.repository.impl;

import com.task.domain.Task;
import com.task.repository.SqlQuery;
import com.task.repository.TaskRepository;
import com.task.rowmapper.TaskRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private TaskRowMapper taskRowMapper;

    @Override
    public Task addTask(Task task) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(con -> {
            PreparedStatement prs = con.prepareStatement(SqlQuery.ADD_TASK, Statement.RETURN_GENERATED_KEYS);
            prs.setLong(1, task.getUser().getId());
            prs.setString(2, task.getTitle());
            prs.setTimestamp(3, Timestamp.valueOf(task.getExpirationDate()));
            return prs;
        }, keyHolder);
        task.setId(Objects.requireNonNull(keyHolder.getKey()).longValue());
        return task;
    }

    @Override
    public List<Task> getAllTaskListLimited(int limit, int offset) {
        Object[] args = new Object[]{limit, offset};
        return jdbcTemplate.query(SqlQuery.GET_ALL_TASK_LIST_LIMITED, args, taskRowMapper);
    }

    @Override
    public List<Task> getAllTaskList() {
        return jdbcTemplate.query(SqlQuery.GET_ACTIVE_STATUS_TASK_LIST,taskRowMapper);
    }

    @Override
    public Integer countAllTasks() {
        return jdbcTemplate.queryForObject(SqlQuery.GET_COUNT_TASK, Integer.class);
    }

    @Override
    public boolean deactivateUser(long userId) {
        Object[] args = new Object[]{userId};
        int value = jdbcTemplate.update(SqlQuery.DEACTIVATE_USER_STATUS, args);
        return value > 0;
    }

    @Override
    public boolean finishTask(long taskId) {
        Object[] args = new Object[]{taskId};
        int value = jdbcTemplate.update(SqlQuery.FINISHE_TASK_STATUS, args);
        return value > 0;
    }

    @Override
    public List<Task> getTaskListByUserId(long userId) {
        Object[] args = new Object[]{userId};
        return jdbcTemplate.query(SqlQuery.GET_TASK_LIST_BY_USER_ID, args, taskRowMapper);
    }

    @Override
    public Optional<Task> getTaskById(long id) {
        Optional<Task> optionalTask = Optional.empty();
        Object[] args = {id};
        List<Task> taskList = jdbcTemplate.query(SqlQuery.GET_TASK_BY_ID, args, taskRowMapper);
        if (!taskList.isEmpty()) {
            optionalTask = Optional.of(taskList.get(0));
        }
        return optionalTask;
    }

    @Override
    public void addBlackList(long userId) {
        Object[] args=new Object[]{userId};
        jdbcTemplate.update(SqlQuery.ADD_BLACK_LIST,args);
    }
}
