package com.task.repository.impl;

import com.task.constant.UserStatus;
import com.task.domain.User;
import com.task.repository.SqlQuery;
import com.task.repository.UserRepository;
import com.task.rowmapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private UserRowMapper userRowMapper;

    @Override
    public User addUser(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(SqlQuery.REGISTER_USER, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getName());
            ps.setString(2, user.getSurName());
            ps.setString(3, user.getUserName());
            ps.setString(4, user.getPassword());
            return ps;
        }, keyHolder);
        user.setId(Objects.requireNonNull(keyHolder.getKey()).longValue());
        return user;
    }

    @Override
    public Optional<User> getUserById(long id) {
        Object[] args = new Object[]{id};
        List<User> userList = jdbcTemplate.query(SqlQuery.USER_LIST_BY_ID, args, userRowMapper);
        Optional<User> userOptional = Optional.empty();
        if (!userList.isEmpty()) {
            userOptional = Optional.of(userList.get(0));
        }
        return userOptional;
    }

    @Override
    public Optional<User> getUserByUSerName(String userName) {
        Object[] args = new Object[]{userName};
        List<User> userList = jdbcTemplate.query(SqlQuery.USER_LIST_BY_USER_NAME, args, userRowMapper);
        Optional<User> userOptional = Optional.empty();
        if (!userList.isEmpty()) {
            userOptional = Optional.of(userList.get(0));
        }
        return userOptional;
    }

    @Override
    public List<User> getDeactiveUserList() {
        List<User> userList = jdbcTemplate.query(SqlQuery.DEACTIVATED_USER_LIST, userRowMapper);
        return userList;
    }

    @Override
    public void activateUser(long id, Timestamp expireDate) {
        Object[]args=new Object[]{id};
        jdbcTemplate.update(SqlQuery.CHANGE_USER_STATUS,args);
    }

    @Override
    public void deleteBlacklistByUserId(long userId) {
        Object[]args=new Object[]{userId};
        jdbcTemplate.update(SqlQuery.DELETE_USER_FROM_BLACKLIST,args);
    }

    @Override
    public void changeTaskStatus(LocalDateTime expirationDate,long userId) {
        Object[]args=new Object[]{expirationDate,userId};
        jdbcTemplate.update(SqlQuery.CHANGE_TASK_STATUS,args);
    }
}
