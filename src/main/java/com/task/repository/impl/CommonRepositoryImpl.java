package com.task.repository.impl;

import com.task.repository.CommonRepository;
import com.task.repository.SqlQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CommonRepositoryImpl implements CommonRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public boolean checkUserName(String userName) {
        Object[] args = new Object[]{userName};
        int count = jdbcTemplate.queryForObject(SqlQuery.CHECK_USERNAME, args, Integer.class);
        return count > 0;
    }

    @Override
    public boolean checkTitle(String title) {
        Object[] args = new Object[]{title};
        int count = jdbcTemplate.queryForObject(SqlQuery.CHECK_TITLE, args, Integer.class);
        return count > 0;
    }


}
