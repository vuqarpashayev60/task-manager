package com.task.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.task.constant.TaskStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Task  implements Serializable {
    private long id;
    private User user;
    private String title;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startingDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime expirationDate;
    private TaskStatus taskStatus;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime idate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime udate;
    private int status;

    public Task() {
        this.user=new User();
        this.title=null;
        this.startingDate=null;
        this.expirationDate=null;
        this.taskStatus=TaskStatus.from(0);
        this.idate=null;
        this.udate=null;
        this.status=1;
    }

    public Task(long id, User user, String title, LocalDateTime startingDate, LocalDateTime expirationDate, TaskStatus taskStatus, LocalDateTime idate, LocalDateTime udate, int status) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.startingDate = startingDate;
        this.expirationDate = expirationDate;
        this.taskStatus = taskStatus;
        this.idate = idate;
        this.udate = udate;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDateTime startingDate) {
        this.startingDate = startingDate;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public LocalDateTime getIdate() {
        return idate;
    }

    public void setIdate(LocalDateTime idate) {
        this.idate = idate;
    }

    public LocalDateTime getUdate() {
        return udate;
    }

    public void setUdate(LocalDateTime udate) {
        this.udate = udate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", user=" + user +
                ", title='" + title + '\'' +
                ", startingDate=" + startingDate +
                ", expirationDate=" + expirationDate +
                ", taskStatus='" + taskStatus + '\'' +
                ", idate=" + idate +
                ", udate=" + udate +
                ", status=" + status +
                '}';
    }
}
