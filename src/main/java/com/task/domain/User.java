package com.task.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.task.constant.UserRole;
import com.task.constant.UserStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

public class User implements Serializable {
    private long id;
    private String name;
    private String surName;
    private String userName;
    private String password;
    private String passwordConfirm;
    private UserStatus userStatus;

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    private UserRole userRole;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime registrationDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime idate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime udate;
    private int status;

    public User() {
        this.name = null;
        this.surName = null;
        this.userName = null;
        this.password = null;
        this.passwordConfirm = getPassword();
        this.userStatus = UserStatus.ACTIVE;
        this.userRole = UserRole.USER;
        this.registrationDate = null;
        this.idate = null;
        this.udate = null;
        this.status = 1;
    }

    public User(long id, String name, String surName, String userName, String password, String passwordConfirm, UserStatus userStatus, LocalDateTime registrationDate, LocalDateTime idate, LocalDateTime udate, int status) {
        this.id = id;
        this.name = name;
        this.surName = surName;
        this.userName = userName;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.userStatus = userStatus;
        this.registrationDate = registrationDate;
        this.idate = idate;
        this.udate = udate;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public LocalDateTime getIdate() {
        return idate;
    }

    public void setIdate(LocalDateTime idate) {
        this.idate = idate;
    }

    public LocalDateTime getUdate() {
        return udate;
    }

    public void setUdate(LocalDateTime udate) {
        this.udate = udate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirm='" + passwordConfirm + '\'' +
                ", userStatus='" + userStatus + '\'' +
                ", registrationDate=" + registrationDate +
                ", idate=" + idate +
                ", udate=" + udate +
                ", status=" + status +
                '}';
    }
}
