package com.task.domain;


import java.io.Serializable;

public class BlackList implements Serializable {
    private long id;
    private User user;

    public BlackList() {
        this.user=new User();
    }

    public BlackList(long id, User user) {
        this.id = id;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "BlackList{" +
                "id=" + id +
                ", user=" + user +
                '}';
    }
}
